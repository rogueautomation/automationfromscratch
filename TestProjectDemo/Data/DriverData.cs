﻿using NUnit.Framework;
using System.Collections.Generic;
using System.IO;
using TestProjectDemo.Helpers;
using TestProjectDemo.Instances;
using TestProjectDemo.Objects;
using TestProjectDemo.Pages;

namespace TestProjectDemo.Data
{
    class DriverData
    {
        public static IEnumerable<TestCaseData> OpenWebPage()
        {
            var webPagesList = new List<WebPage>
            {
                WebPageInstances.Google,
                WebPageInstances.YouTube,
                WebPageInstances.Facebook
            };

            foreach(WebPage webPage in webPagesList)
            {
                yield return new TestCaseData(webPage)
                    .SetName($"OpenWebPage({webPage})")
                    .SetDescription($"Validating Selenium Webdriver 4 is able to successfully open {webPage}")
                    .SetCategory($"TestRail Case ID: Refers the manual test case in testrail");
            }
        }

        public static IEnumerable<TestCaseData> CheckWebPageHttpResponse()
        {
            var list = HelperReader.CSVReader(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName + "/Resources/Autowatch Websites.csv");

            foreach (string webPage in list)
            {
                yield return new TestCaseData(webPage)
                    .SetName($"CheckWebPageHttpResponse({webPage})")
                    .SetDescription($"Validating Selenium Webdriver 4 is able to successfully open {webPage}")
                    .SetCategory($"TestRail Case ID: Refers the manual test case in testrail");
            }
        }
    }
}
