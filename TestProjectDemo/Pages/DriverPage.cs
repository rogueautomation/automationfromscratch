﻿using NUnit.Framework;
using RestSharp;
using TestProjectDemo.Objects;
using TestProjectDemo.Setup;

namespace TestProjectDemo.Pages
{
    public class DriverPage : App
    {
        public static void NavigatesTo(WebPage webPage)
        {
            driver.Navigate().GoToUrl(webPage.url);
            ValidatesPageWasOpened(webPage);
        }

        private static void ValidatesPageWasOpened(WebPage webPage)
        {
            fluentWait.Until(x => x.FindElement(webPage.selector));
            Assert.Pass();
        }

        public static void CheckHTTPResponse(string url)
        {
            IRestResponse restResponse = new RestClient($"http://{url}").Get(new RestRequest("/"));
            Assert.IsTrue(restResponse.IsSuccessful || restResponse.StatusCode.ToString() == "ServiceUnavailable", $"{restResponse.StatusCode}");
        }
    }
}
