﻿using System.Collections.Generic;
using System.IO;

namespace TestProjectDemo.Helpers
{
    class HelperReader
    {
        public static List<string> CSVReader(string path)
        {
            var reader = new StreamReader(path);
            List<string> listA = new List<string>();
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                var values = line.Split(',');

                listA.Add(values[0].Trim());
            }

            return listA;
        }
    }
}
