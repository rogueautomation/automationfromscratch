using NUnit.Framework;
using TestProjectDemo.Data;
using TestProjectDemo.Objects;
using TestProjectDemo.Pages;
using TestProjectDemo.Setup;

namespace TestProjectDemo.Tests.UI
{
    public class DriverTests
    {
        [SetUp]
        public void Setup()
        {
            _ = new App();
        }

        [Test, Author("Rajiv Moguel"), TestCaseSource(typeof(DriverData), nameof(DriverData.OpenWebPage))]
        [Category("Smoke"), Category("Driver"), Category("UI")]
        [Parallelizable(ParallelScope.All)]
        public void OpenWebPage(WebPage webPage)
        {
            DriverPage.NavigatesTo(webPage);
        }

        [TearDown]
        public void Teardown()
        {
            DriverPage.Dispose();
        }
    }
}