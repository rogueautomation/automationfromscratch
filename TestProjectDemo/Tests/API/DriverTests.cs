using NUnit.Framework;
using TestProjectDemo.Data;
using TestProjectDemo.Pages;

namespace TestProjectDemo.Tests.API
{
    public class DriverTests
    {
        [Test, Author("Rajiv Moguel"), TestCaseSource(typeof(DriverData), nameof(DriverData.CheckWebPageHttpResponse))]
        [Category("Smoke"), Category("Driver"), Category("API")]
        [Parallelizable(ParallelScope.All)]
        public void CheckWebPageHttpResponse(string url)
        {
            DriverPage.CheckHTTPResponse(url);
        }
    }
}