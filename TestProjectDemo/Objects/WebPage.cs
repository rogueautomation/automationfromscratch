﻿using OpenQA.Selenium;

namespace TestProjectDemo.Objects
{
    public class WebPage
    {
        public string name { get; set; }
        public string url { get; set; }
        public By selector { get; set; }

        public override string ToString()
        {
            return name;
        }
    }
}
