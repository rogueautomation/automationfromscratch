﻿using Microsoft.Extensions.Configuration;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System;
using System.IO;

namespace TestProjectDemo.Setup
{
    public class App
    {
        [ThreadStatic]
        protected static IWebDriver driver = null;
        [ThreadStatic]
        protected static DefaultWait<IWebDriver> fluentWait = null;

        public App()
        {
            // Driver options and capabilities.
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.AddArgument("start-maximized");
            chromeOptions.AddArgument("--disable-backgrounding-occluded-windows");
            chromeOptions.AddArgument("--ignore-certificate-errors");
            chromeOptions.AddArgument("test-type");
            chromeOptions.AddArgument("disable-infobars");

            // Local vs Remote execution condition.
            if (bool.Parse(GetSetting("RemoteRun").Value))
                driver = new RemoteWebDriver(new Uri(GetSetting("RemoteExecutorURL").Value), chromeOptions);
            else
                driver = new ChromeDriver(chromeOptions);
            
            // Wait properties.
            fluentWait = new DefaultWait<IWebDriver>(driver);
            fluentWait.Timeout = TimeSpan.FromSeconds(10);
            fluentWait.PollingInterval = TimeSpan.FromMilliseconds(250);
            fluentWait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            fluentWait.Message = "Element to be searched not found";

        }

        private IConfigurationSection GetSetting(string settingName)
        {
            var path = Directory.GetCurrentDirectory();
            var env = Directory.GetParent(path).Name;
            path = Directory.GetParent(path).Parent.Parent.FullName;

            IConfigurationRoot rawSetting = new ConfigurationBuilder()
                .SetBasePath(path)
                .AddJsonFile("appsettings.json", false, true)
                .AddJsonFile($"appsettings.{env}.json", true, true)
                .Build();

            return rawSetting.GetSection(settingName);
        }

        public static void Dispose()
        {
            driver.Close();
            driver.Dispose();
        }

    }
}
