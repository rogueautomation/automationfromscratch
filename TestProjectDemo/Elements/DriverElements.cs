﻿using OpenQA.Selenium;

namespace TestProjectDemo.Elements
{
    public class DriverElements
    {
        public static By GoogleLogo { get; } = By.ClassName("lnXdpd");
        public static By YouTubeLogo { get; } = By.XPath("//ytd-topbar-logo-renderer[@id='logo']");
        public static By FacebookLogo { get; } = By.XPath("//*[@class='fb_logo _8ilh img']");
    }
}
