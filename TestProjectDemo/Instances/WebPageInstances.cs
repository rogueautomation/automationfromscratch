﻿using TestProjectDemo.Elements;
using TestProjectDemo.Objects;

namespace TestProjectDemo.Instances
{
    public class WebPageInstances
    {
        public static WebPage Google = new WebPage
        {
            name = "Google",
            url = "https://google.com",
            selector = DriverElements.GoogleLogo
        };

        public static WebPage YouTube = new WebPage
        {
            name = "YouTube",
            url = "https://youtube.com",
            selector = DriverElements.YouTubeLogo
        };

        public static WebPage Facebook = new WebPage
        {
            name = "Facebook",
            url = "https://facebook.com",
            selector = DriverElements.FacebookLogo
        };
    }
}
